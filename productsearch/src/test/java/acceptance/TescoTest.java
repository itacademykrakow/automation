package acceptance;

import org.testng.annotations.Test;
import tests.BaseSearchTest;

public class TescoTest extends BaseSearchTest {

  public TescoTest() {
    super(TescoTest.class);
  }

  @Test
  public void auchan() {
    navigateToAuchanPage().getProductsInfo();
  }
}
