package acceptance;

import org.testng.annotations.Test;
import tests.BaseSearchTest;

public class AuchanTest extends BaseSearchTest {

  public AuchanTest() {
    super(AuchanTest.class);
  }

  @Test
  public void auchan() {
    navigateToAuchanPage().getProductsInfo();
  }
}
