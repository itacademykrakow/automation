package acceptance;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import scanners.AuchanProductScanner;

public class AuchanProductTest extends AuchanProductScanner {

  public AuchanProductTest() {
    super(AuchanProductTest.class);
  }

  @DataProvider(name = "auchanProduct")
  public static Object[][] createData() {
    return new Object[][] {
      {
        "https://www.auchandirect.pl/auchan-warszawa/pl/selenka-wieniec-zdroj-naturalna-woda-mineralna-niegazowana/p-98503917?fromCategory=true"
      }
    };
  }

  @Test(dataProvider = "auchanProduct")
  public void auchan(String url) throws Exception {
    run(url);
  }
}
