package utils.auchan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AuchanUtil {

  public static int getShopId() {
    return 1;
  }


  public static String getProductUrlPart(String url) {
    Pattern p = Pattern.compile("[?&/]p-([0-9]+)");
    Matcher m = p.matcher(url);
    m.find();
    return m.group(1);
  }
}



