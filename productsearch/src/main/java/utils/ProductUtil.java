package utils;

import entities.ProductWeight;
import entities.auchan.AuchanProduct;
import enums.Measure;

public class ProductUtil {
  private static String logFile = "auchan.log";

  public static void write(AuchanProduct product) {
    try {
      FileUtil.writeToFile(logFile, product);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static ProductWeight convert(double weight, String measure) {
    double resultWeight = weight;
    Measure resultMeasure = null;
    switch (measure.trim().toLowerCase()) {
      case "kg":
        resultMeasure = Measure.KG;
        break;
      case "g":
        resultWeight = resultWeight / 1000;
        resultMeasure = Measure.KG;
        break;
      case "l":
        resultMeasure = Measure.LITR;
        break;
    }
    return new ProductWeight(resultWeight, resultMeasure);
  }
}
