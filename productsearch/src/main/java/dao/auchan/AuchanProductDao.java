package dao.auchan;

import db.DbConnection;
import entities.auchan.AuchanProduct;
import enums.DB;
import enums.Measure;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class AuchanProductDao {
  private static String dbName = DB.DEV.getName();

  private static String getProductIdByUrlSql = "SELECT id, product_id FROM prices WHERE url = :url";

  private static String getProductInfoByUrlSql =
      "select \n"
          + "products.id, \n"
          + "products.name, \n"
          + "products.weight, \n"
          + "products.measure,\n"
          + "prices.id as price_id,\n"
          + "prices.orig_name,\n"
          + "prices.shop_id,\n"
          + "prices.price,\n"
          + "prices.url,\n"
          + "prices.photo_url,\n"
          + "prices.added,\n"
          + "prices.updated\n"
          + "FROM prices\n"
          + "JOIN products\n"
          + "ON prices.product_id = products.id\n"
          + "WHERE prices.url = :url";

  private static String insertProductInfoInProductsSql =
      "INSERT\n"
          + "INTO products\n"
          + "(name, weight, measure)\n"
          + "VALUES (:name, :weight, :measure)\n";

  private static String updateProductInfoInProductsSql =
      "UPDATE products SET weight=?, measure=? WHERE id=?";

  private static String insertProductInfoInPricesSql =
      "INSERT\n"
          + "INTO prices\n"
          + "(orig_name, shop_id, price, url, photo_url, product_id)\n"
          + "VALUES (:orig_name, :shop_id, :price, :url, :photo_url, :product_id)\n";

  private static String updateProductInfoInPricesSql =
      "UPDATE prices\n" + "SET orig_name=?,\n" + "price=?,\n" + "photo_url=?\n" + "WHERE id=?";

  public static Integer getProductIdByUrl(String url) {
    MapSqlParameterSource parameters = new MapSqlParameterSource();
    parameters.addValue("url", url);
    return DbConnection.selectInt(getProductIdByUrlSql, parameters, dbName);
  }

  private static void insertProductToProducts(AuchanProduct auchanProduct) {
    int productId =
        DbConnection.insert(
            insertProductInfoInProductsSql,
            new MapSqlParameterSource()
                .addValue("name", auchanProduct.getOrigName())
                .addValue("weight", auchanProduct.getWeight())
                .addValue("measure", auchanProduct.getMeasure().getDbName()),
            dbName);
    auchanProduct.setId(productId);
  }

  private static void updateProductInProducts(AuchanProduct auchanProduct) {
    DbConnection.update(
        updateProductInfoInProductsSql,
        dbName,
        new Object[] {
          auchanProduct.getWeight(), auchanProduct.getMeasure().getDbName(), auchanProduct.getId()
        });
  }

  private static void insertProductToPrices(AuchanProduct auchanProduct) {
    int priceId =
        DbConnection.insert(
            insertProductInfoInPricesSql,
            new MapSqlParameterSource()
                .addValue("orig_name", auchanProduct.getOrigName())
                .addValue("shop_id", auchanProduct.getShopId())
                .addValue("price", auchanProduct.getPrice())
                .addValue("url", auchanProduct.getUrl())
                .addValue("photo_url", auchanProduct.getPhotoUrl())
                .addValue("product_id", auchanProduct.getId()),
            dbName);
    auchanProduct.setPriceId(priceId);
  }

  private static void updateProductInPrices(AuchanProduct auchanProduct) {
    DbConnection.update(
        updateProductInfoInPricesSql,
        dbName,
        new Object[] {
          auchanProduct.getOrigName(),
          auchanProduct.getPrice(),
          auchanProduct.getPhotoUrl(),
          auchanProduct.getId()
        });
  }

  public static void insertProduct(AuchanProduct product) {
    insertProductToProducts(product);
    insertProductToPrices(product);
  }

  public static void updateProduct(AuchanProduct product) {
    updateProductInProducts(product);
    updateProductInPrices(product);
  }

  public static AuchanProduct getProductByUrl(String url) throws SQLException {
    MapSqlParameterSource parameters = new MapSqlParameterSource();
    parameters.addValue("url", url);
    return DbConnection.selectObject(
        getProductInfoByUrlSql, parameters, getProductRowMap(), dbName);
  }

  private static RowMapper<AuchanProduct> getProductRowMap() throws SQLException {
    RowMapper<AuchanProduct> mapper =
        new RowMapper<AuchanProduct>() {
          @Override
          public AuchanProduct mapRow(ResultSet rs, int i) throws SQLException {
            final AuchanProduct product = new AuchanProduct();
            product.setId(rs.getInt("id"));
            product.setGivenName(rs.getString("name"));
            product.setMeasure(Measure.fromDBName(rs.getString("measure")));
            product.setPriceId(rs.getInt("price_id"));
            product.setOrigName(rs.getString("orig_name"));
            product.setShopId(rs.getInt("shop_id"));
            product.setPrice(rs.getDouble("price"));
            product.setUrl(rs.getString("url"));
            product.setPhotoUrl(rs.getString("photo_url"));
            product.setAdded(rs.getDate("added"));
            product.setUpdated(rs.getDate("updated"));
            return product;
          }
        };
    return mapper;
  }
}
