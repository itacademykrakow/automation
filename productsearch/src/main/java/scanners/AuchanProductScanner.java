package scanners;

import dao.auchan.AuchanProductDao;
import entities.auchan.AuchanProduct;
import pages.auchan.AuchanProductPage;
import tests.BaseSearchTest;

public class AuchanProductScanner extends BaseSearchTest {

  private String url;

  public AuchanProductScanner(Class clazz) {
    super(clazz);
  }

  public AuchanProductScanner(String url) throws Exception {
    this(url, null);
  }

  public AuchanProductScanner(String url, String config) throws Exception {
    super(url);
    this.url = url;
    BaseSearchTest.initSystem(config);
  }

  public AuchanProductScanner run() throws Exception {
    run(url);
    return this;
  }

  public void run(String url) throws Exception {
    getDriver().navigateTo(url);
    AuchanProduct auchanProduct = null;

    //TODO check if loginForm is visible
    auchanProduct = new AuchanProductPage(getDriver()).getProduct();

    System.out.println(auchanProduct.toString());
    AuchanProduct product = AuchanProductDao.getProductByUrl(auchanProduct.getUrl());
    if (product == null) {
      AuchanProductDao.insertProduct(auchanProduct);
    } else {
      AuchanProductDao.updateProduct(auchanProduct);
    }
  }

  public void close() {
    closeDriver();
  }
}
