package scanners;

import java.io.File;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;

public class CommandLineManager {

  public static void main(String[] args) {
    Options options = new Options();
    options.addOption(
        OptionBuilder.withArgName("productUrl")
            .hasArg()
            .withDescription("use given url for scanning")
            .create("url"));
    options.addOption(
        OptionBuilder.withArgName("configPath")
            .hasArg()
            .withDescription("use given config for scanning")
            .create("config"));
    CommandLineParser parser = new DefaultParser();
    try {
      CommandLine cmd = parser.parse(options, args);
      if (!cmd.hasOption("url")) {
        printErr("No correct option or not supported option yet.");
        return;
      }
      String url = cmd.getOptionValue("url");
      if (!url.contains("auchandirect")) {
        printErr("Not supported url yet.");
        return;
      }
      AuchanProductScanner scanner;
      if (cmd.hasOption("config")) {
        scanner = new AuchanProductScanner(url, cmd.getOptionValue("config"));
      } else {
        File jarDir = new File(
            CommandLineManager.class.getProtectionDomain().getCodeSource().getLocation().toURI()
                .getPath());
        scanner = new AuchanProductScanner(url, jarDir.getParent() + "/config.txt");
      }
      scanner.run().close();
      print("Auchan product scanned successfully!");

    } catch (Exception e) {
      printErr(e.getMessage());
    }
  }

  private static void printErr(String str) {
    System.out.print("ERROR: " + str);
  }

  private static void print(String str) {
    System.out.print(str);
  }
}
