package entities.auchan;

import entities.Product;
import entities.ProductWeight;
import utils.auchan.AuchanUtil;

public class AuchanProduct extends Product {

  public AuchanProduct() {}

  public AuchanProduct(
      String name, double price, String url, String photoUrl, ProductWeight productWeight) {
    super(name, AuchanUtil.getShopId(), price, url, photoUrl, productWeight);
  }

  @Override
  public String toString() {
    return super.toString();
  }
}
