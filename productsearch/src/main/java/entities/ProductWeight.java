package entities;

import enums.Measure;

public class ProductWeight {
  private double weight;
  private Measure measure;

  public ProductWeight(double weight, Measure measure) {
    this.weight = weight;
    this.measure = measure;
  }

  public double getWeight() {
    return weight;
  }

  public Measure getMeasure() {
    return measure;
  }
}
