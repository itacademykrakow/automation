package entities;

import enums.Measure;
import java.util.Date;

public class Product {

  private int id;
  private int priceId;
  private String origName;
  private int shopId;
  private double price;
  private String url;
  private String photoUrl;
  private double weight;
  private Measure measure;
  private String givenName;
  private Date added;
  private Date updated;

  public Product() {}

  public Product(
      String origName,
      int shopId,
      double price,
      String url,
      String photoUrl,
      ProductWeight productWeight) {
    this.origName = origName;
    this.shopId = shopId;
    this.price = price;
    this.url = url;
    this.photoUrl = photoUrl;
    this.weight = productWeight.getWeight();
    this.measure = productWeight.getMeasure();
  }

  public String getOrigName() {
    return origName;
  }

  public int getShopId() {
    return shopId;
  }

  public double getPrice() {
    return price;
  }

  public String getUrl() {
    return url;
  }

  public String getPhotoUrl() {
    return photoUrl;
  }

  public double getWeight() {
    return weight;
  }

  public Measure getMeasure() {
    return measure;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getPriceId() {
    return priceId;
  }

  public void setPriceId(int priceId) {
    this.priceId = priceId;
  }

  public String getGivenName() {
    return givenName;
  }

  public void setGivenName(String givenName) {
    this.givenName = givenName;
  }

  public void setOrigName(String origName) {
    this.origName = origName;
  }

  public void setShopId(int shopId) {
    this.shopId = shopId;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public void setPhotoUrl(String photoUrl) {
    this.photoUrl = photoUrl;
  }

  public void setWeight(double weight) {
    this.weight = weight;
  }

  public void setMeasure(Measure measure) {
    this.measure = measure;
  }

  public Date getAdded() {
    return added;
  }

  public void setAdded(Date added) {
    this.added = added;
  }

  public Date getUpdated() {
    return updated;
  }

  public void setUpdated(Date updated) {
    this.updated = updated;
  }

  @Override
  public String toString() {
    return "Product{"
        + "id="
        + id
        + ", priceId="
        + priceId
        + ", origName='"
        + origName
        + '\''
        + ", shopId="
        + shopId
        + ", price="
        + price
        + ", url='"
        + url
        + '\''
        + ", photoUrl='"
        + photoUrl
        + '\''
        + ", weight="
        + weight
        + ", measure="
        + measure
        + '}';
  }
}
