package enums;

public enum DB {
  DEV("baza23129_zakupy");
  private String name;

  DB(String name) {
    this.name = name;
  }

  public String getName() {
    return this.name;
  }
}
