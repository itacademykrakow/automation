package enums;

public enum Measure {
  KG("kg"),
  LITR("l"),
  NUMBER("szt");
  private String dbName;

  Measure(String dbName) {
    this.dbName = dbName;
  }

  public String getDbName() {
    return this.dbName;
  }

  public static Measure fromDBName(String dbName) {
    for (Measure measure : Measure.values()) {
      if (measure.getDbName().equals(dbName)) return measure;
    }
    return null;
  }
}
