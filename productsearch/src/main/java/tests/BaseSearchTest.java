package tests;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import pages.auchan.AuchanPage;

public class BaseSearchTest extends BaseTest {

  @BeforeSuite
  public void before() throws Exception {
    super.before();
  }

  @AfterTest
  public void aftert() {
    super.aftert();
  }

  public BaseSearchTest(Class clazz) {
    super(clazz);
  }

  public BaseSearchTest(String testName) {
    super(testName);
  }

  public static void initSystem(String configPath) throws Exception {
    BaseTest.initSystem(configPath);
  }

  protected AuchanPage navigateToAuchanPage() {
    getDriver().navigateTo("https://www.auchandirect.pl/auchan-warszawa/pl/");
    return new AuchanPage(getDriver());
  }
}
