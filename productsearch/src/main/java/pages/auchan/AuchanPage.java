package pages.auchan;

import drivers.BaseDriver;
import java.util.List;
import java.util.stream.Collectors;
import org.openqa.selenium.By;
import pages.BasePage;
import utils.SleepUtil;

public class AuchanPage extends BasePage {

  private static String categoriesXpath = "//a[@class='level3--title trigger']";

  public AuchanPage(BaseDriver driver) {
    super(driver, "Zakupy spożywcze przez Internet – Zbijamy ceny zawodowo online – Sklep Auch");
  }

  private List<String> readAllCategoriesUrl() {
    return driver
        .getWait()
        .getElements(By.xpath(categoriesXpath))
        .stream()
        .map(c -> c.getAttribute("href"))
        .collect(Collectors.toList());
  }

  private AuchanCategoryPage navigateToCategory(String url) {
    driver.navigateTo(url);
    return new AuchanCategoryPage(driver);
  }

  public void getProductsInfo() {
    List<String> categoryUrls = readAllCategoriesUrl();
    for (String categoryUrl : categoryUrls) {
      navigateToCategory(categoryUrl).getAllProducts();
      SleepUtil.sleep(2);
    }
  }
}
