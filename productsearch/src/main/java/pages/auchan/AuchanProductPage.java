package pages.auchan;

import controls.WebImage;
import controls.WebLabel;
import drivers.BaseDriver;
import entities.ProductWeight;
import entities.auchan.AuchanProduct;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import pages.BasePage;
import utils.ProductUtil;
import utils.auchan.AuchanUtil;

public class AuchanProductPage extends BasePage {

  private static String nameXpath = "//h1[@class = 'title']";
  private static String weightXpath = "//p[@class = 'packaging']/strong";
  private static String priceXpathZL = "//span[@class = 'p-nb']";
  private static String priceXpathGR = "//span[@class = 'p-cents']";
  private static String photoXpath = "//div[@class='product-visual']//img";
  private WebLabel name, priceZl, priceGr, weight;
  private WebImage photo;

  public AuchanProductPage(BaseDriver driver) {
    super(driver);
    name = new WebLabel("AuchanProduct Name", nameXpath, driver);
    priceZl = new WebLabel("Price in zl", priceXpathZL, driver);
    priceGr = new WebLabel("Price in gr", priceXpathGR, driver);
    weight = new WebLabel("AuchanProduct Weight", weightXpath, driver);
    photo = new WebImage("AuchanProduct photo", photoXpath, driver);
  }

  private String getProductName() {
    return name.getText();
  }

  private double getProductPrice() {
    return Double.parseDouble(priceZl.getText() + "." + priceGr.getText());
  }

  private ProductWeight getProductWeight() throws NumberFormatException, ParseException {
    String prodWeight = weight.getText(1);
    String[] tmp = prodWeight.split("(?<=\\d)\\s*(?=[a-zA-Z])");
    NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
    Number number = format.parse(tmp[0]);
    return ProductUtil.convert(number.doubleValue(), tmp[1]);
  }

  private String getProductUrl() {
    return AuchanUtil.getProductUrlPart(driver.getWebDriver().getCurrentUrl());
  }

  private String getProductPhotoUrl() {
    return photo.getRelativeUrl();
  }

  public AuchanProduct getProduct() throws ParseException {
    //String name, double price, String url, String photoUrl, double weight, Measure measure
    return new AuchanProduct(
        getProductName(),
        getProductPrice(),
        getProductUrl(),
        getProductPhotoUrl(),
        getProductWeight());
  }
}
