package pages.auchan;

import controls.WebButton;
import controls.WebLabel;
import controls.WebLink;
import drivers.BaseDriver;
import org.openqa.selenium.By;
import pages.BasePage;
import utils.SleepUtil;

public class AuchanCategoryPage extends BasePage {

  private WebButton loadMore =
      new WebButton("Load more", By.xpath("//button[@id = 'loadMoreProductsButton']"), driver);
  private static String sectionXpath = "//section[@class = 'page page-list']";
  private static String productXpath = "//*[normalize-space(@class)='product']";
  private static String nameXpath = "//a[@class = 'about']";
  private static String priceXpathZL = "//span[@class = 'p-nb']";
  private static String priceXpathGR = "//span[@class = 'p-cents']";
  private static String weightXpath = "//p[@class = 'packaging']/strong";
  private String fullNameXpath = "(" + sectionXpath + productXpath + ")[%d]" + nameXpath;
  private String fullPriceXpathZL = "(" + sectionXpath + productXpath + ")[%d]" + priceXpathZL;
  private String fullPriceXpathGR = "(" + sectionXpath + productXpath + ")[%d]" + priceXpathGR;
  private String fullWeightXpath = "(" + sectionXpath + productXpath + ")[%d]" + weightXpath;

  public AuchanCategoryPage(BaseDriver driver) {
    super(driver);
  }

  public void getAllProducts() {
    return;
    /*loadAll();
    int size = getProductSize();
    List<AuchanProduct> products = new ArrayList<>();

    for (int i = 0; i < size; i++) {
      String productUrl = getProductUrl(i + 1);
      String productName = getProductName(i + 1);
      String productPrice = getProductPrice(i + 1);
      String productWeight = getProductWeight(i + 1);
      AuchanProduct product =
          new AuchanProduct(productName, productPrice, productWeight, productUrl);
      products.add(product);
      ProductUtil.write(product);
      //TODO insert to database
    }
    */
  }

  private String getProductWeight(int i) {
    String productWeight;
    try {
      productWeight =
          new WebLabel("AuchanProduct Weight", String.format(fullWeightXpath, i), driver)
              .getText(1);
    } catch (Exception e) {
      productWeight = "szt";
    }
    return productWeight;
  }

  private String getProductUrl(int i) {
    return new WebLink("AuchanProduct Url", String.format(fullNameXpath, i), driver).getUrl();
  }

  private String getProductName(int i) {
    return new WebLabel("AuchanProduct Name", String.format(fullNameXpath, i), driver).getText();
  }

  private String getProductPrice(int i) {
    return new WebLabel("Price in zl", String.format(fullPriceXpathZL, i), driver).getText()
        + "."
        + new WebLabel("Price in gr", String.format(fullPriceXpathGR, i), driver).getText();
  }

  private void loadAll() {
    while (loadMore.isClickable(1)) {
      loadMore.click(true);
      SleepUtil.sleep(2);
    }
  }

  private int getProductSize() {
    return driver.getWait().getElements(By.xpath(sectionXpath + productXpath)).size();
  }
}
