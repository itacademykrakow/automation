package pages.tesco;

import controls.WebButton;
import controls.WebLabel;
import controls.WebLink;
import drivers.BaseDriver;
import org.openqa.selenium.By;
import pages.BasePage;

public class TescoCategoryPage extends BasePage {

  private WebButton loadMore =
      new WebButton(
          "Load more", By.xpath("//a[@aria-label='Przejdź do następnej strony']"), driver);
  private static String sectionXpath = "//div[@class='product-list-container']";
  private static String productXpath = "//div[@class='product-tile--wrapper']";
  private static String nameXpath = "//a[@class='product-tile--title product-tile--browsable']";
  // static String priceXpathZL = "//span[@class = 'p-nb']";
  private static String PriceXpathTesco =
      "//div[@class='price-per-sellable-unit price-per-sellable-unit--price price-per-sellable-unit--price-per-weight']//span[@class='value']";
  //private static String priceXpathGR = "//span[@class = 'p-cents']";
  private static String TescoWeightXpath = "";
  private String fullNameXpath = "(" + sectionXpath + productXpath + ")[%d]" + nameXpath;
  private String fullPriceXpathTesco =
      "(" + sectionXpath + productXpath + ")[%d]" + PriceXpathTesco;
  //private String fullPriceXpath = "(" + sectionXpath + productXpath + ")[%d]" + priceXpathGR;
  //private String fullWeightXpath = "(" + sectionXpath + productXpath + ")[%d]" + weightXpath;

  public TescoCategoryPage(BaseDriver driver) {
    super(driver);
  }

  public void getAllProducts() {
    return;
    /*
    while (loadMore.isClickable(1)) {

      int size = getProductSize();

      List<AuchanProduct> products = new ArrayList<>();

      for (int i = 0; i < size; i++) {
        String productUrl = getProductUrl(i + 1);
        String productName = getProductName(i + 1);
        String productPrice = getProductPrice(i + 1);
        //String productWeight = getProductWeight(i + 1);
        AuchanProduct product = new AuchanProduct(productName, productPrice, "", productUrl);
        products.add(product);
        try {
          FileUtil.writeToFile("tesco.log", product);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
      loadMore.click(true);
      SleepUtil.sleep(1);
    }
    */
  }

  /*private String getProductWeight(int i) {
      String productWeight;
      try {
        productWeight =
            new WebLabel("AuchanProduct Weight", String.format(fullWeightXpath, i), driver).getText(1);
      } catch (Exception e) {
        productWeight = "szt";
      }
      return productWeight;
    }
  */
  private String getProductUrl(int i) {
    return new WebLink("AuchanProduct Url", String.format(fullNameXpath, i), driver).getUrl();
  }

  private String getProductName(int i) {
    return new WebLabel("AuchanProduct Name", String.format(fullNameXpath, i), driver).getText();
  }

  private String getProductPrice(int i) {
    return new WebLabel("Price in zl", String.format(fullPriceXpathTesco, i), driver).getText();
  }

  /* private void loadAll() {
      while (loadMore.isClickable(1)) {
        loadMore.click(true);
        SleepUtil.sleep(1);
      }
    }
  */
  private int getProductSize() {
    return driver.getWait().getElements(By.xpath(sectionXpath + productXpath)).size();
  }
}
