package pages.tesco;

import drivers.BaseDriver;
import java.util.List;
import java.util.stream.Collectors;
import org.openqa.selenium.By;
import pages.BasePage;
import utils.SleepUtil;

public class TescoPage extends BasePage {

  private static String categoriesXpath = "//span[@class='menu-item-name']";

  public TescoPage(BaseDriver driver) {
    super(driver, "Homepage");
  }

  private List<String> readAllCategoriesUrl() {
    return driver
        .getWait()
        .getElements(By.xpath(categoriesXpath))
        .stream()
        .map(c -> c.getAttribute("href"))
        .collect(Collectors.toList());
  }

  private TescoCategoryPage navigateToCategory(String url) {
    driver.navigateTo(url);
    return new TescoCategoryPage(driver);
  }

  public void getProductsInfo() {
    List<String> categoryUrls = readAllCategoriesUrl();
    for (String categoryUrl : categoryUrls) {
      navigateToCategory(categoryUrl).getAllProducts();
      SleepUtil.sleep(2);
    }
  }
}
