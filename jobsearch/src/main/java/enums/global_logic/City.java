package enums.global_logic;

public enum City {

    KRK("Krakow"),
    ALL("All");


    private String name;

    City(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
