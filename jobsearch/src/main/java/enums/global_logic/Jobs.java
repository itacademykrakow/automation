package enums.global_logic;

import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum Jobs {

    //    Array is a temp solution text for searching words will be replaces to properties.
    QA_AUTOMATION(Stream.of("QA Engineer",
            "Automaton tester",
            "Developer for test automation").collect(Collectors.toList()));

    private List<String> jobs;

    Jobs(List<String> jobs) {
        this.jobs = jobs;
    }

    public boolean isOnString(String job) {
        for (String searchedJob : jobs){
            if (StringUtils.containsIgnoreCase(job, searchedJob)) return true;
        }
        return false;
    }
}
