package enums.global_logic;

public enum Roles {

    TESTQA("Quality Assurance"),
    ALL("All");

    private String name;

    Roles(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
