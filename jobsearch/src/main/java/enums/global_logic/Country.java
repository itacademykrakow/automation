package enums.global_logic;

public enum Country {

    PL("Poland");

    private String name;

    Country(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
