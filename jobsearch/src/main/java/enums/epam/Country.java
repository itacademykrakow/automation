package enums.epam;

public enum Country {
    PL("Poland");

    private String name;
    Country(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
