package enums.luxsoft;

public enum City {

    KRK("Krakow"),
    WRC("Wroclaw");

    String name;

    City(String name) {
        this.name = name;
    }

    @Override
    public String toString (){
        return name;
    }

}
