package enums.luxsoft;

public enum Roles {

    QAAUTO("QA automation");

    private String name;


    Roles(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
