package enums.luxsoft;

public enum Country {

    PL("Poland");

    String name;


    Country(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
