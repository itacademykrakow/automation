package pages.epam;


import controls.WebButton;
import controls.WebDropDown;
import drivers.BaseDriver;
import enums.epam.Country;
import org.openqa.selenium.By;
import pages.DirectAccessPage;

import java.util.List;

public class EpamCareersPage extends DirectAccessPage {
    public static String url = "https://www.epam.com/careers";
    private WebDropDown location;
    private WebDropDown roles;

    private WebButton search;

    public EpamCareersPage(BaseDriver driver) {
        super(driver, "Careers");
        location = new WebDropDown("Location", "//div[@class='dropdown-position-wrapper']//div[contains(@class,'select-box-container')]", "//*[@aria-label='%s']", "//*[contains(@id,'all_Poland')]", driver);
        roles = new WebDropDown("Roles", "//div[@class='selected-params']", "//input[@data-value='%s']/following-sibling::span", null, driver);

        search = new WebButton("Search", By.name("Search"), driver);
    }

    public EpamCareersPage findByCountry(Country country) {
        location.selectByText(country.getName());
        return this;

    }

    public EpamCareersPage findByRole(List<String> roleNames) {
        roles.selectByText(roleNames);
        return this;
    }

    public EpamJobListings submit() {
        search.click();
        return new EpamJobListings(driver);
    }
}
