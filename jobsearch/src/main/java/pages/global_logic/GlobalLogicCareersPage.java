package pages.global_logic;

import controls.WebButton;
import controls.WebDropDown;
import drivers.BaseDriver;
import enums.global_logic.City;
import enums.global_logic.Country;
import enums.global_logic.Roles;
import org.openqa.selenium.By;
import pages.DirectAccessPage;
import tables.global_logic.GlobalLogicCareerTable;

public class GlobalLogicCareersPage extends DirectAccessPage {

    public static String url = "https://www.globallogic.com/pl/careers/";
    private WebDropDown location;
    private WebDropDown city;
    private WebDropDown role;
    private WebButton findJobs;

    public GlobalLogicCareersPage findByCountry(Country country) {
        location.selectByText(country.toString());
        return this;
    }

    public GlobalLogicCareersPage findByCity(City c) {
        city.selectByText(c.toString());
        return this;
    }

    public GlobalLogicCareersPage findByRole(Roles r) {
        role.selectByText(r.toString());
        return this;
    }


    public GlobalLogicCareersPage(BaseDriver driver) {
        super(driver, "Best Employer in Poland | IT Jobs | Globallogic Poland");
        location = new WebDropDown("country", "//select[@name = 'country']", "//option[@value = '%s']", null, driver);
        city = new WebDropDown("City", "//select[@name = 'office']", "//*[@value = '%s']", null, driver);
        role = new WebDropDown("Role", "//select[@name = 'job_category']", "//*[@value = '%s']", null, driver);
        findJobs = new WebButton("Find Jobs", By.xpath("//*[@type = 'submit']"), driver);
    }


    public GlobalLogicJobListings submit() {
        findJobs.click();
        return new GlobalLogicJobListings(driver);
    }
}
