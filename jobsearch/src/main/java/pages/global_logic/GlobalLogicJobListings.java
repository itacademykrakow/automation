package pages.global_logic;

import drivers.BaseDriver;
import pages.BasePage;

public class GlobalLogicJobListings extends BasePage {

    public GlobalLogicJobListings(BaseDriver driver) {
        super(driver, "Best Employer in Poland | IT Jobs | Globallogic Poland");
    }

}
