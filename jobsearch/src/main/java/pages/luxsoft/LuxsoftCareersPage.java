package pages.luxsoft;

import controls.WebButton;
import controls.WebDropDown;
import drivers.BaseDriver;
import enums.luxsoft.City;
import enums.luxsoft.Country;
import enums.luxsoft.Roles;
import org.openqa.selenium.By;
import pages.DirectAccessPage;

public class LuxsoftCareersPage extends DirectAccessPage {


    public static String url = "https://career.luxoft.com/job-opportunities/";
    private WebDropDown location;
    private WebDropDown city;
    private WebDropDown role;
    private WebButton findJobs;

    public LuxsoftCareersPage findByCountry(Country country) {
        location.selectByText(country.toString());
        return this;
    }

    public LuxsoftCareersPage findByCity(City c) {
        city.selectByText(c.toString());
        return this;
    }

    public LuxsoftCareersPage findByRole(Roles r) {
        role.selectByText(r.toString());
        return this;
    }


    public LuxsoftCareersPage(BaseDriver driver) {
        super(driver, "Job opportunities - Software developers | Luxoft Company");
        location = new WebDropDown("Country", "//*[@id='search_countries']/following-sibling::span", "//li[text()='%s']", null, driver);
        city = new WebDropDown("City", "//*[@id='search_cities']/following-sibling::span", "//*[@value = '%s']", null, driver);
//        role = new WebDropDown("Role", "//*[@id='search_type']/following-sibling::span", "//*[@title = '%s']", null, driver);
        role = new WebDropDown("Role", "//*[@id='search_type']/following-sibling::span", "//*[text() = '%s']", null, driver);

        findJobs = new WebButton("Find Jobs", By.xpath("//button[@type = 'submit']"), driver);

    }


    public LuxsoftCareersPage submit() {
        findJobs.click();
        return new LuxsoftCareersPage(driver);
    }
}


