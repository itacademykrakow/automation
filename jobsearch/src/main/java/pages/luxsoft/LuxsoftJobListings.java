package pages.luxsoft;

import drivers.BaseDriver;
import pages.BasePage;

public class LuxsoftJobListings extends BasePage {


    public LuxsoftJobListings(BaseDriver driver) {
        super(driver, "Job Listings");
    }
}
