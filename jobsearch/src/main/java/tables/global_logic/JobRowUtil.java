package tables.global_logic;

import java.util.List;

public class JobRowUtil {
    public static void print(List<GlobalLogicCareerRow> rows) {
        for (GlobalLogicCareerRow row : rows) {
            System.out.println("Job Title: " + row.getJobName() +
                    " Link: " + row.toString() +
                    " Localization: " + row.getLocalization() +
                    " Days: " + row.getDaysOfOffer());
        }
    }
}
