package tables.global_logic;

import drivers.BaseDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class GlobalLogicCareerRow {

    private final BaseDriver driver;
    private WebElement webElement;
    private List<WebElement> cells;
    private static final int JOB_NAME = 0;
    private static final int LOCALIZATION = 1;
    private static final int DAYS_OFFER = 2;

    public GlobalLogicCareerRow(BaseDriver driver, WebElement webElement) {
        this.driver = driver;
        this.webElement = webElement;
        cells = webElement.findElements(By.xpath("./child::*"));
    }

    @Override
    public String toString() {
        return webElement.getAttribute("href");
    }

    public String getJobName() {
        return cells.get(JOB_NAME).getText();
    }

    public String getLocalization(){
        return cells.get(LOCALIZATION).getText();
    }

    public String getDaysOfOffer(){
        return cells.get(DAYS_OFFER).getText();
    }
}
