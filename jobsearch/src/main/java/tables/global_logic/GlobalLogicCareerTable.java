package tables.global_logic;

import controls.WebButton;
import controls.WebLabel;
import drivers.BaseDriver;
import enums.global_logic.Jobs;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import pages.global_logic.GlobalLogicCareersPage;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GlobalLogicCareerTable {

    private final BaseDriver driver;
    private WebButton loadMore;
    private WebLabel loadingText;
    private static final String tableXpath = "//div[@class= 'career_pg_list_of_job_order']//a";
    private static final String loadMoreXpath = "(//div[@class= 'career_pg_load_more_btn_main'])[last()]";
    private static final String loadingTextXpath = "//a[@class = 'inifiniteLoader']";

    public GlobalLogicCareerTable(BaseDriver driver) {
        this.driver = driver;
        loadMore = new WebButton("Load More", By.xpath(loadMoreXpath), driver);
    }

    public List<GlobalLogicCareerRow> getRows(Jobs job) {
        clickLoadMore();
        return driver.getWait()
                .getElements(By.xpath(tableXpath))
                .stream()
                .map(element -> new GlobalLogicCareerRow(driver, element))
                .filter(row -> job.isOnString(row.getJobName()))
                .collect(Collectors.toList());
    }

    public void clickLoadMore(){
        while (loadMore.isVisible()){
            loadMore = new WebButton("Load More", By.xpath(loadMoreXpath), driver);
            loadingText = new WebLabel("Loading...", By.xpath(loadingTextXpath), driver);
            loadMore.click();
            waitLoading();
        }
    }

    private void waitLoading(){
        while (loadingText.isVisible()){
//            Change to wait method add waitUtilVisible
        }
    }

}
