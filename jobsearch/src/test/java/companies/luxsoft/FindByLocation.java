package companies.luxsoft;

import enums.luxsoft.Country;
import enums.luxsoft.Roles;
import org.testng.annotations.Test;
import pages.luxsoft.LuxsoftCareersPage;
import tests.BaseTest;


import static interfaces.PageNavigator.getPage;

public class FindByLocation extends BaseTest {

    public FindByLocation() {
        super(companies.luxsoft.FindByLocation.class);
    }

    @Test
    public void poland() throws Exception {
        getPage(getDriver(), LuxsoftCareersPage.class)
                .findByCountry(Country.PL)
                .findByRole(Roles.QAAUTO)
                .submit();
    }

}
