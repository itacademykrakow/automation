package companies.epam;

import enums.epam.Country;
import org.testng.annotations.Test;
import pages.epam.EpamCareersPage;
import tests.BaseTest;

import java.util.Arrays;

import static interfaces.PageNavigator.getPage;

public class FindByLocation extends BaseTest {

    public FindByLocation() {
        super(FindByLocation.class);
    }

    @Test
    public void poland() throws Exception {

        getPage(getDriver(), EpamCareersPage.class)
                .findByCountry(Country.PL)
                .findByRole(Arrays.asList("Software Engineering", "Software Test Engineering"))
                .submit();
    }
}

