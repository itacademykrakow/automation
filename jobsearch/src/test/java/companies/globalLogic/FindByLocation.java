package companies.globalLogic;

        import enums.global_logic.City;
        import enums.global_logic.Country;
        import enums.global_logic.Jobs;
        import enums.global_logic.Roles;
        import org.testng.annotations.Test;
        import pages.global_logic.GlobalLogicCareersPage;
        import pages.global_logic.GlobalLogicJobListings;
        import tables.global_logic.GlobalLogicCareerRow;
        import tables.global_logic.GlobalLogicCareerTable;
        import tables.global_logic.JobRowUtil;
        import tests.BaseTest;

        import static interfaces.PageNavigator.getPage;

public class FindByLocation extends BaseTest {

    public FindByLocation() {
        super(FindByLocation.class);
    }

    @Test
    public void poland() throws Exception {

                getPage(getDriver(), GlobalLogicCareersPage.class)
                        .findByCountry(Country.PL)
                        .findByRole(Roles.TESTQA)
                        .submit();

        GlobalLogicCareerTable careerTable = new GlobalLogicCareerTable(getDriver());
        JobRowUtil.print(careerTable.getRows(Jobs.QA_AUTOMATION));

    }
}
