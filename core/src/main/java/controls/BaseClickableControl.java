package controls;

import drivers.BaseDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.WebElement;

public class BaseClickableControl extends BaseControl {

  public BaseClickableControl(String name, By locator, BaseDriver driver) {
    super(name, locator, driver);
  }

  public WebElement getClickableElement() {
    return getClickableElement(driver.getTimeout());
  }

  private WebElement getClickableElement(int sec) {
    WebElement e = driver.getWait().getClickableElement(locator, sec);
    if (e == null)
      throw new ElementNotInteractableException(
          "Element with locator " + locator + " is NOT clickable.");
    return e;
  }

  public void click() {
    click(false);
  }

  public void click(boolean js) {
    WebElement e = getClickableElement();
    if (!js) {
      e.click();
    } else {
      driver.getJsDriver().executeScript("arguments[0].click();", e);
    }
  }

  public void check() {
    WebElement e = getClickableElement();
    if (!e.isSelected()) {
      e.click();
    }
  }

  public boolean isClickable() {
    return isClickable(driver.getTimeout());
  }

  public boolean isClickable(int sec) {
    try {
      WebElement e = getClickableElement(sec);
      if (e == null) {
        return false;
      }
    } catch (Exception e) {
      return false;
    }
    return true;
  }
}
