package controls;

import drivers.BaseDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

/**
 * Created by ja on 09.07.2017.
 */
public class WebDropDown extends BaseClickableControl {

    private String elementLocator;
    private String subElementLocator;

    public WebDropDown(String name, String locator, String elementLocator, String subElementLocator, BaseDriver driver) {
        super(name, By.xpath(locator), driver);
        this.elementLocator = elementLocator;
        this.subElementLocator = this.elementLocator + subElementLocator;
    }

    public void selectByText(String text) {
        WebElement dropDown = driver.getWait().getClickableElement(locator);
        dropDown.click();
        WebLink subElement = new WebLink("SubElement", By.xpath(String.format(subElementLocator, text)), driver);
        if (subElementLocator == null || (subElementLocator != null && !subElement.isClickable(1))) {
            new WebLink("Element", By.xpath(String.format(elementLocator, text)), driver).click();
        }
    }

    public void selectByText(List<String> list) {
        WebElement dropDown = driver.getWait().getClickableElement(locator);
        dropDown.click();

        for (String s : list) {
            new WebCheckBox("Role", By.xpath(String.format(elementLocator, s)), driver).check();
        }
    }

    public List<WebElement> getAllOptions() {
        Select dropDown = new Select(driver.getWait().getClickableElement(locator));
        List<WebElement> options = dropDown.getOptions();
        return options;
    }

    public String getSelectedText() {
        Select dropDown = new Select(driver.getWait().getClickableElement(locator));
        List<WebElement> options = dropDown.getOptions();
        for (WebElement option : options) {
            if (option.isSelected()) {
                return option.getText();
            }
        }
        return null;
    }
}
