package controls;

import drivers.BaseDriver;
import org.openqa.selenium.By;

public class WebImage extends BaseClickableControl {

  public WebImage(String name, By locator, BaseDriver driver) {
    super(name, locator, driver);
  }

  public WebImage(String name, String locator, BaseDriver driver) {
    super(name, By.xpath(locator), driver);
  }

  public String getUrl() {
    return driver.getWait().getVisibleElement(locator).getAttribute("src");
  }

  public String getRelativeUrl() {
    return driver
        .getJsDriver()
        .executeScript(
            "return arguments[0].attributes['src'].value;",
            driver.getWait().getVisibleElement(locator))
        .toString();
  }
}
