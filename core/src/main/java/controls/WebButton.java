package controls;

import drivers.BaseDriver;
import org.openqa.selenium.By;

public class WebButton extends BaseClickableControl {

  public WebButton(String name, By locator, BaseDriver driver) {
    super(name, locator, driver);
  }

  public WebButton(String name, String locator, BaseDriver driver) {
    super(name, By.xpath(locator), driver);
  }
}
