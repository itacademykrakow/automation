package controls;

import drivers.BaseDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;

public class BaseControl {

  protected String name;
  protected By locator;
  protected BaseDriver driver;

  public BaseControl(String name, By locator, BaseDriver driver) {
    this.name = name;
    this.locator = locator;
    this.driver = driver;
  }

  @Deprecated
  public WebElement getElement() {
    return driver.getWebDriver().findElement(locator);
  }

  public WebElement getVisibleElement() {
    return getVisibleElement(driver.getTimeout());
  }

  public WebElement getVisibleElement(int sec) {
    WebElement e = driver.getWait().getVisibleElement(locator, sec);
    if (e == null)
      throw new ElementNotVisibleException("Element with locator " + locator + " is NOT visible.");
    return e;
  }

  public String getText() {
    return getText(driver.getTimeout());
  }

  public String getText(int sec) {
    return getVisibleElement(sec).getText();
  }

  public boolean isVisible() {
    return isVisible(driver.getTimeout());
  }

  public boolean isVisible(int sec) {
    try {
      WebElement e = driver.getWait().getVisibleElement(locator, sec);
      if (e == null) {
        return false;
      }
    } catch (Exception e) {
      return false;
    }
    return true;
  }
}
