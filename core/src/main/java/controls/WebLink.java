package controls;

import drivers.BaseDriver;
import org.openqa.selenium.By;

public class WebLink extends BaseClickableControl {

  public WebLink(String name, By locator, BaseDriver driver) {
    super(name, locator, driver);
  }

  public WebLink(String name, String locator, BaseDriver driver) {
    super(name, By.xpath(locator), driver);
  }

  public String getUrl() {
    return getClickableElement().getAttribute("href");
  }
}
