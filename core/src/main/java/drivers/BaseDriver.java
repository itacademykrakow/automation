package drivers;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import waiters.WebWaiter;

public class BaseDriver {

  protected WebDriver webDriver;
  protected WebWaiter wait;
  protected int timeout = 10;

  public BaseDriver() {
    wait = new WebWaiter();
  }

  public void navigateTo(String url) {
    this.webDriver.get(url);
    this.webDriver.manage().window().maximize();
  }

  public WebDriver getWebDriver() {
    return webDriver;
  }

  public WebWaiter getWait() {
    return wait;
  }

  public JavascriptExecutor getJsDriver() {
    return (JavascriptExecutor) webDriver;
  }

  public int getTimeout() {
    return timeout;
  }
}
