package drivers;

import org.openqa.selenium.chrome.ChromeDriver;

public class ChromeDriverWrapper extends BaseDriver {
  public ChromeDriverWrapper() {
    super();
    this.webDriver = new ChromeDriver();
    wait.init(this);
  }
}
