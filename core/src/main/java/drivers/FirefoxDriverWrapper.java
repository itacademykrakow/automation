package drivers;

import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxDriverWrapper extends BaseDriver {
  public FirefoxDriverWrapper() {
    super();
    this.webDriver = new FirefoxDriver();
    wait.init(this);
  }
}
