package verifiers;

import drivers.BaseDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public class Verifier {

  protected BaseDriver driver;

  public boolean verifyIsExist(final By locator) {
    try {
      driver.getWait().getElement(locator);
    } catch (NoSuchElementException ex) {
      return false;
    }
    return true;
  }

  public boolean verifyIsVisible(final By locator) {
    try {
      WebElement e = driver.getWait().getElement(locator);
      if (e.isDisplayed()) {
        return true;
      }
    } catch (NoSuchElementException ex) {
      return false;
    }
    return false;
  }

  public boolean verifyIsNotVisible(final By locator) {
    try {
      WebElement e = driver.getWait().getElement(locator);
      if (!e.isDisplayed()) {
        return true;
      }
    } catch (NoSuchElementException ex) {
      return false;
    }
    return false;
  }
}
