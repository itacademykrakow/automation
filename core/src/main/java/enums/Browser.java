package enums;

public enum Browser {
  FIREFOX("firefox"),
  CHROME("chrome");
  private String name;

  Browser(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public static Browser fromName(String name) {
    for (Browser browser : Browser.values()) {
      if (browser.getName().equals(name)) {
        return browser;
      }
    }
    return null;
  }
}
