package db;

import com.mysql.cj.jdbc.MysqlConnectionPoolDataSource;
import entities.DbConfig;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

public class DbConnection {
  private static Map<String, NamedParameterJdbcTemplate> pool = new HashMap<>();

  public static void init(List<DbConfig> dbConfigList) throws SQLException {
    pool.clear();
    for (DbConfig config : dbConfigList) {
      if (!pool.containsKey(config.getDb())) {
        MysqlConnectionPoolDataSource con = new MysqlConnectionPoolDataSource();
        con.setUrl(
            "jdbc:mysql://"
                + config.getHost()
                + "/"
                + config.getDb()
                + "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Europe/Warsaw");
        con.setUser(config.getUsername());
        con.setPassword(config.getPass());
        pool.put(config.getDb(), new NamedParameterJdbcTemplate((DataSource) con));
      }
    }
  }

  public static NamedParameterJdbcTemplate getConnection(String name) {
    if (pool.isEmpty()) {
      throw new IndexOutOfBoundsException("Connections are not initialized yet.");
    }
    return pool.get(name);
  }

  public static void query(String sql, String name) {
    getConnection(name).getJdbcOperations().execute(sql);
  }

  public static void update(String sql, String name) {
    getConnection(name).getJdbcOperations().update(sql);
  }

  public static void update(String sql, String name, Object[] param) {
    getConnection(name).getJdbcOperations().update(sql, param);
  }

  public static String selectString(String sql, String name) {
    return selectString(sql, new MapSqlParameterSource(), name);
  }

  private static String selectString(String sql, MapSqlParameterSource param, String name) {
    return getConnection(name).queryForObject(sql, param, String.class);
  }

  public static Date selectDate(String sql, String name) {
    return selectDate(sql, new MapSqlParameterSource(), name);
  }

  private static Date selectDate(String sql, MapSqlParameterSource param, String name) {
    return getConnection(name).queryForObject(sql, param, Date.class);
  }

  public static Integer selectInt(String sql, String name) {
    return selectInt(sql, new MapSqlParameterSource(), name);
  }

  public static Integer selectInt(String sql, MapSqlParameterSource param, String name) {
    try {
      return getConnection(name).queryForObject(sql, param, Integer.class);
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  public static Double selectDouble(String sql, String name) {
    return selectDouble(sql, new MapSqlParameterSource(), name);
  }

  private static Double selectDouble(String sql, MapSqlParameterSource param, String name) {
    return getConnection(name).queryForObject(sql, param, Double.class);
  }

  public static <T> List<T> selectList(String sql, RowMapper<T> rowMapper, String name) {
    return selectList(sql, new MapSqlParameterSource(), rowMapper, name);
  }

  public static <T> List<T> selectList(
      String sql, MapSqlParameterSource param, RowMapper<T> rowMapper, String name) {
    return getConnection(name).query(sql, param, rowMapper);
  }

  public static int insert(String sql, String name) {
    return insert(sql, new MapSqlParameterSource(), name);
  }

  public static int insert(String sql, MapSqlParameterSource param, String name) {
    KeyHolder keyHolder = new GeneratedKeyHolder();
    getConnection(name).update(sql, param, keyHolder);
    return keyHolder.getKey().intValue();
  }

  public static Map<String, Object> selectMap(String sql, String name) {
    return selectMap(sql, new MapSqlParameterSource(), name);
  }

  public static Map<String, Object> selectMap(
      String sql, MapSqlParameterSource param, String name) {
    try {
      return getConnection(name).queryForMap(sql, param);
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  public static <T> T selectObject(String sql, RowMapper<T> rowMapper, String name) {
    return selectObject(sql, new MapSqlParameterSource(), rowMapper, name);
  }

  public static <T> T selectObject(
      String sql, MapSqlParameterSource param, RowMapper<T> rowMapper, String name) {
    try {
      return getConnection(name).queryForObject(sql, param, rowMapper);
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }
}
