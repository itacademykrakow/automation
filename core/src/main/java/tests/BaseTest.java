package tests;

import db.DbConnection;
import drivers.BaseDriver;
import drivers.ChromeDriverWrapper;
import drivers.FirefoxDriverWrapper;
import entities.PropertiesConfig;
import enums.Browser;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.apache.commons.io.FileUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import utils.PropertiesUtils;

public class BaseTest {

  private BaseDriver driver;
  private String testName;
  private static String chro = "webdriver.chrome.driver";
  private static String fire = "webdriver.gecko.driver";

  @BeforeSuite
  public void before() throws Exception {
    initSystem();
  }

  public static void initSystem() throws Exception {
    initSystem(null);
  }

  public static void initSystem(String configPath) throws Exception {
    PropertiesConfig config = PropertiesUtils.read(configPath);
    Files.createDirectories(Paths.get(config.getOutput()));
    FileUtils.cleanDirectory(new File(PropertiesUtils.getOutput()));
    if (config.getBrowser().getName().equals("firefox")) {
      System.setProperty(fire, config.getPath());
    } else {
      System.setProperty(chro, config.getPath());
    }
    DbConnection.init(config.getDbConfigList());
  }

  @AfterTest
  public void aftert() {
    closeDriver();
  }

  public void closeDriver() {
    if (driver != null) {
      driver.getWebDriver().quit();
      driver = null;
    }
  }

  public BaseTest(Class clazz) {
    this(clazz.getName());
  }

  public BaseTest(String testName) {
    this.testName = testName;
  }

  protected BaseDriver getDriver() {
    if (driver == null) {
      PropertiesConfig config = PropertiesUtils.read();
      this.driver = initDriver(config.getBrowser());
    }
    return this.driver;
  }

  private static BaseDriver initDriver(Browser browser) {
    BaseDriver curDriver;
    switch (browser) {
      case FIREFOX:
        curDriver = new FirefoxDriverWrapper();
      default:
        curDriver = new ChromeDriverWrapper();
    }
    return curDriver;
  }
}
