package utils;

import entities.DbConfig;
import entities.PropertiesConfig;
import enums.Browser;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.apache.commons.lang3.StringUtils;
import tests.BaseTest;

public class PropertiesUtils {

  private static PropertiesConfig config;
  private static String defaultConfig = "webProperties.properties";

  public static PropertiesConfig read() {
    return read(null);
  }

  public static PropertiesConfig read(String configPath) {
    if (config == null) {
      config = new PropertiesConfig();
      Properties prop = new Properties();
      InputStream input = null;

      try {
        if (StringUtils.isBlank(configPath)) {
          input = BaseTest.class.getClassLoader().getResourceAsStream(defaultConfig);
        } else {
          input = new FileInputStream(configPath);
        }
        if (input == null) {
          throw new FileNotFoundException();
        }
        prop.load(input);

        if (StringUtils.isBlank(prop.getProperty("path"))) {
          config.setPath(new File(configPath).getParent() + "/webdrivers/chromedriver.exe");
        } else {
          config.setPath(prop.getProperty("path"));
        }

        config.setBrowser(Browser.fromName(prop.getProperty("browser")));

        if (StringUtils.isBlank(prop.getProperty("output"))) {
          config.setOutput(new File(configPath).getParent() + "/logs");
        } else {
          config.setOutput(prop.getProperty("output"));
        }

        config.addConfig(
            new DbConfig(
                prop.getProperty("db"),
                prop.getProperty("dbhost"),
                prop.getProperty("dbuser"),
                prop.getProperty("dbpass")));

      } catch (IOException ex) {
        ex.printStackTrace();
      } finally {
        if (input != null) {
          try {
            input.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }
      return config;
    }
    return config;
  }

  public static String getOutput() {
    return config.getOutput();
  }
}
