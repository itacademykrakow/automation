package utils;

import static org.apache.commons.codec.CharEncoding.UTF_8;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class FileUtil {

  public static void writeToFile(String fileName, Object o) throws Exception {
    writeToFile(fileName, o.toString());
  }

  public static void writeToFile(String fileName, String content) throws Exception {
    Path p = getFullPath(fileName);
    Files.write(p, content.getBytes(UTF_8), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
  }

  private static Path getFullPath(String fileName) {
    return Paths.get(PropertiesUtils.getOutput(), fileName);
  }
}
