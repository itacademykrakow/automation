package utils.restClient;

import java.io.IOException;
import java.nio.charset.Charset;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class RestHttpClient {

  public static RestResponse get(String url) {
    return get(new RestRequest(url));
  }

  public static RestResponse get(RestRequest request) {
    CloseableHttpClient httpclient = HttpClients.createDefault();
    HttpGet httpGet = new HttpGet(request.getUrl());
    try {
      if (request.getParams() != null) {
        //Uri uri = new URIBuilder(httpGet.getURI()).addParameters().
        //httpGet.setURI(httpGet.getURI();
      }

      CloseableHttpResponse response = httpclient.execute(httpGet);
      String body = StringUtils
          .join(IOUtils.readLines(response.getEntity().getContent(), Charset.defaultCharset()), "");
      return new RestResponse(response.getStatusLine().getStatusCode(), body);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static RestResponse post(String url) {
    CloseableHttpClient httpclient = HttpClients.createDefault();
    HttpPost httpGet = new HttpPost(url);
    try {
      CloseableHttpResponse response = httpclient.execute(httpGet);
      String body = StringUtils
          .join(IOUtils.readLines(response.getEntity().getContent(), Charset.defaultCharset()), "");
      return new RestResponse(response.getStatusLine().getStatusCode(), body);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }
}
