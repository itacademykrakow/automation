package utils.restClient;

import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class RestRequest {

  private String url;
  private List<NameValuePair> params;

  public RestRequest(String url) {
    this.url = url;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public List<NameValuePair> getParams() {
    return params;
  }

  public void setParams(List<NameValuePair> params) {
    this.params = params;
  }

  public List<NameValuePair> addParam(String key, String value) {
    if (params == null) {
      params = new ArrayList<>();
    }
    params.add(new BasicNameValuePair(key, value));
    return params;
  }
}
