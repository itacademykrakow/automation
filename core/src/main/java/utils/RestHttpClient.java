package utils;

import java.io.IOException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class RestHttpClient {

  public static void get(String url) {
    CloseableHttpClient httpclient = HttpClients.createDefault();
    HttpGet httpGet = new HttpGet(url);
    try {
      CloseableHttpResponse response1 = httpclient.execute(httpGet);
      System.out.print("");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
