package entities;

import enums.Browser;
import java.util.ArrayList;
import java.util.List;

public class PropertiesConfig {

  private Browser browser;
  private String path;
  private List<DbConfig> dbConfigList = new ArrayList<>();
  private String output;

  public Browser getBrowser() {
    return browser;
  }

  public void setBrowser(Browser browser) {
    this.browser = browser;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public List<DbConfig> getDbConfigList() {
    return dbConfigList;
  }

  public List<DbConfig> addConfig(DbConfig config) {
    dbConfigList.add(config);
    return dbConfigList;
  }

  public String getOutput() {
    return output;
  }

  public void setOutput(String output) {
    this.output = output;
  }
}
