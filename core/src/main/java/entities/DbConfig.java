package entities;

public class DbConfig {

  private String db;
  private String host;
  private String username;
  private String pass;

  public DbConfig(String db, String host, String username, String pass) {
    this.db = db;
    this.host = host;
    this.username = username;
    this.pass = pass;
  }

  public String getDb() {
    return db;
  }

  public String getHost() {
    return host;
  }

  public String getUsername() {
    return username;
  }

  public String getPass() {
    return pass;
  }
}
