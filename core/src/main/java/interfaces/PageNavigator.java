package interfaces;

import drivers.BaseDriver;
import pages.DirectAccessPage;

public interface PageNavigator {
  static <T extends DirectAccessPage> T getPage(BaseDriver driver, Class<T> clazz)
      throws Exception {
    driver.navigateTo(clazz.getDeclaredField("url").get(null).toString());
    return clazz.getDeclaredConstructor(BaseDriver.class).newInstance(driver);
  }
}
