package waiters;

import drivers.BaseDriver;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

public class WebWaiter {

  private BaseDriver driver;
  private FluentWait<WebDriver> wait;

  public WebWaiter() {
  }

  public void init(BaseDriver driver) {
    this.driver = driver;
    wait = new FluentWait<WebDriver>(driver.getWebDriver());
    wait.pollingEvery(100, TimeUnit.MILLISECONDS);
    wait.withTimeout(10, TimeUnit.SECONDS);
    wait.ignoring(NoSuchElementException.class);
  }

  public WebElement getElement(final By locator) {
    return wait.until(ExpectedConditions.presenceOfElementLocated(locator));
  }

  public List<WebElement> getElements(final By locator) {
    return wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
  }

  public WebElement getVisibleElement(final By locator) {
    return getVisibleElement(locator, driver.getTimeout());
  }

  public WebElement getVisibleElement(final By locator, int sec) {
    wait.withTimeout(sec, TimeUnit.SECONDS);
    return wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
  }

  public WebElement getClickableElement(final By locator) {
    return getClickableElement(locator, driver.getTimeout());
  }

  public WebElement getClickableElement(final By locator, int sec) {
    wait.withTimeout(sec, TimeUnit.SECONDS);
    return wait.until(ExpectedConditions.elementToBeClickable(locator));
  }

  public boolean verifyTitle(String title) {
    return wait.until(ExpectedConditions.titleIs(title));
  }

}
