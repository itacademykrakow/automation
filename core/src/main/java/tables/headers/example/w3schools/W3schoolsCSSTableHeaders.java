package tables.headers.example.w3schools;

import tables.TableHeader;

public enum W3schoolsCSSTableHeaders implements TableHeader {
  COMPANY("Company"),
  CONTACT("Contact"),
  COUNTRY("Country");

  private String name;

  W3schoolsCSSTableHeaders(String name) {
    this.name = name;
  }

  @Override
  public String getName() {
    return this.name;
  }
}
