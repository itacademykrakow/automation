package tables;

import controls.BaseControl;
import drivers.BaseDriver;
import java.util.List;

public class BaseRow {
  private BaseDriver driver;
  private List<String> headers;
  private String xpath;
  private String cellLocator;

  public BaseRow(BaseDriver driver, List<String> headers, String xpath) {
    this.driver = driver;
    this.headers = headers;
    this.xpath = xpath;
    this.cellLocator = "/td[%d]";
  }

  public <T extends BaseControl> T createControl(Class<T> clazz, TableHeader header)
      throws Exception {
    int n = headers.indexOf(header.getName());
    return clazz
        .getConstructor(String.class, String.class, BaseDriver.class)
        .newInstance(
            header.getName(), String.format(this.xpath + this.cellLocator, n + 1), this.driver);
  }
}
