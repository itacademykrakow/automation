package tables;

import drivers.BaseDriver;
import java.util.List;
import org.openqa.selenium.By;

public class BaseTable<
    T extends BaseRow, H extends BaseHeaderRow, E extends Enum<E> & TableHeader> {

  private BaseDriver driver;
  private String baseTableXpath;
  private String rowXpath;
  private String headerXpath;
  private String headerCellXpath;
  private String bodyRowLocator;
  private String theaderLocator;
  private String tbodyLocator;
  private String rowCellLocator;
  private String headerCellLocator;
  private Class<T> rowClass;
  private H headerRow;
  private Class<H> headerClass;

  public BaseTable(
      BaseDriver driver,
      String baseXpath,
      String headerRowXpath,
      String bodyRowXpath,
      String headerXpath,
      String tbodyXpath,
      String rowCellXpath,
      String headerCellXpath,
      Class<H> baseHeaderRowClass,
      Class<T> row) {
    this.driver = driver;
    this.baseTableXpath = baseXpath;
    this.rowXpath = baseXpath + tbodyXpath + bodyRowXpath;
    this.headerXpath = baseXpath + headerXpath + headerRowXpath;
    this.headerCellXpath = String.format("(%s)[1]", this.headerXpath) + headerCellXpath;
    this.bodyRowLocator = bodyRowXpath;
    this.theaderLocator = headerXpath;
    this.tbodyLocator = tbodyXpath;
    this.rowCellLocator = rowCellXpath;
    this.headerCellLocator = headerCellXpath;
    this.rowClass = row;
    this.headerClass = baseHeaderRowClass;
  }

  public T getFirstRow() {
    String rowLocator = "(" + rowXpath + ")[1]";
    return driver.getWait().getVisibleElement(By.xpath(rowLocator)) == null
        ? null
        : createRow(rowLocator);
  }

  private T createRow(String rowLocator) {
    try {
      return rowClass
          .getConstructor(BaseDriver.class, List.class, String.class)
          .newInstance(driver, getHeaderNames(), rowLocator);
    } catch (Exception e) {
      return null;
    }
  }

  private List<String> getHeaderNames() {
    return getHeaderRow().getHeaders();
  }

  private H getHeaderRow() {
    if (headerRow == null) {
      try {
        headerRow =
            headerClass
                .getConstructor(BaseDriver.class, String.class)
                .newInstance(driver, headerCellXpath);
      } catch (Exception e) {
        return null;
      }
    }
    return headerRow;
  }
}
