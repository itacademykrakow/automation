package tables.example.w3schools;

import drivers.BaseDriver;
import tables.AbstractTable;
import tables.headers.example.w3schools.W3schoolsCSSTableHeaders;
import tables.rows.example.w3schools.W3schoolsCSSTableRow;

public class W3schoolsCSSTable
    extends AbstractTable<W3schoolsCSSTableRow, W3schoolsCSSTableHeaders> {
  private static final String TABLE_XPATH = "//table[@id='customers']";
  private static final String HEADER_XPATH = "";
  private static final String HEADER_ROW_XPATH = "//tr[1]";
  private static final String HEADER_CELL_XPATH = "/th";
  private static final String TBODY_XPATH = "/tbody/";
  private static final String BODY_ROW_XPATH = "tr[position()>1]";
  private static final String ROW_CELL_XPATH = "/td";

  public W3schoolsCSSTable(BaseDriver driver) {
    super(
        driver,
        TABLE_XPATH,
        W3schoolsCSSTableHeaders.COMPANY,
        HEADER_ROW_XPATH,
        BODY_ROW_XPATH,
        HEADER_XPATH,
        TBODY_XPATH,
        ROW_CELL_XPATH,
        HEADER_CELL_XPATH,
        W3schoolsCSSTableRow.class);
  }
}
