package tables;

import drivers.BaseDriver;

public class AbstractTable<T extends BaseRow, E extends Enum<E> & TableHeader>
    extends BaseTable<T, BaseHeaderRow, E> {
  private static final String TABLE_XPATH = "//th[contains(.,'%s')]/ancestor::table[1]";
  private static final String HEADER_XPATH = "/thead/";
  private static final String HEADER_ROW_XPATH = "tr[1]";
  private static final String HEADER_CELL_XPATH = "/th";
  private static final String TBODY_XPATH = "/tbody/";
  private static final String BODY_ROW_XPATH = "tr[position()>1]";
  private static final String ROW_CELL_XPATH = "/td";

  public AbstractTable(BaseDriver driver, TableHeader header, Class<T> row) {
    super(
        driver,
        String.format(TABLE_XPATH, header.getName()),
        HEADER_ROW_XPATH,
        BODY_ROW_XPATH,
        HEADER_XPATH,
        TBODY_XPATH,
        ROW_CELL_XPATH,
        HEADER_CELL_XPATH,
        BaseHeaderRow.class,
        row);
  }

  public AbstractTable(
      BaseDriver driver,
      String tableXpath,
      TableHeader header,
      String headerRowXpath,
      String bodyRowXpath,
      String headerXpath,
      String tbodyXpath,
      String rowCellXpath,
      String headerCellXpath,
      Class<T> row) {
    super(
        driver,
        String.format(tableXpath, header.getName()),
        headerRowXpath,
        bodyRowXpath,
        headerXpath,
        tbodyXpath,
        rowCellXpath,
        headerCellXpath,
        BaseHeaderRow.class,
        row);
  }
}
