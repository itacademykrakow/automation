package tables;

import drivers.BaseDriver;
import java.util.List;
import java.util.stream.Collectors;
import org.openqa.selenium.By;

public class BaseHeaderRow {

  private BaseDriver driver;
  private String cellLocator;
  private List<String> headers;

  public BaseHeaderRow(BaseDriver driver, String cellLocator) {
    this.driver = driver;
    this.cellLocator = cellLocator;
    this.headers = getHeaderNames();
  }

  public List<String> getHeaders() {
    return headers;
  }

  public List<String> getHeaderNames() {
    return driver
        .getWait()
        .getElements(By.xpath(cellLocator))
        .stream()
        .map(e -> e.getText().trim())
        .collect(Collectors.toList());
  }
}
