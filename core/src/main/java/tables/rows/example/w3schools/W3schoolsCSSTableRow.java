package tables.rows.example.w3schools;

import controls.WebLabel;
import drivers.BaseDriver;
import java.util.List;
import tables.BaseRow;
import tables.headers.example.w3schools.W3schoolsCSSTableHeaders;

public class W3schoolsCSSTableRow extends BaseRow {
  private WebLabel company;
  private WebLabel contact;
  private WebLabel country;

  public W3schoolsCSSTableRow(BaseDriver driver, List<String> headers, String xpath)
      throws Exception {
    super(driver, headers, xpath);
    company = createControl(WebLabel.class, W3schoolsCSSTableHeaders.COMPANY);
    contact = createControl(WebLabel.class, W3schoolsCSSTableHeaders.CONTACT);
    country = createControl(WebLabel.class, W3schoolsCSSTableHeaders.COUNTRY);
  }

  public String getCompany() {
    return company.getText();
  }

  public String getContact() {
    return contact.getText();
  }

  public String getCountry() {
    return country.getText();
  }

  @Override
  public String toString() {
    return "W3schoolsCSSTableRow{"
        + "company="
        + getCompany()
        + ", contact="
        + getContact()
        + ", country="
        + getCountry()
        + '}';
  }
}
