package pages;

import drivers.BaseDriver;

public class BasePage {

  protected final BaseDriver driver;

  @Deprecated
  public BasePage(BaseDriver driver) {
    this.driver = driver;
  }

  public BasePage(BaseDriver driver, String title) {
    this.driver = driver;
    driver.getWait().verifyTitle(title);
  }
}
