package pages.example.itacademy;

import controls.WebLink;
import drivers.BaseDriver;
import org.openqa.selenium.By;
import pages.DirectAccessPage;

public class ITAcademyHomePage extends DirectAccessPage {

  public static String url = "http://it-academy.pl";
  private WebLink szkolenia;

  public ITAcademyHomePage(BaseDriver driver) {
    super(driver, "Szkoła Programowania Kraków - Kursy Programowania | IT-Academy");
    szkolenia =
        new WebLink(
            "Szkolenia link",
            By.xpath("//div[contains(@class, 'header_main_menu_wrapper')]//a[text()='Szkolenia']"),
            driver);
  }

  public void navigateToSzkolenia() {
    szkolenia.click();
  }
}
