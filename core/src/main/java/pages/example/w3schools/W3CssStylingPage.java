package pages.example.w3schools;

import drivers.BaseDriver;
import pages.DirectAccessPage;
import tables.example.w3schools.W3schoolsCSSTable;
import tables.rows.example.w3schools.W3schoolsCSSTableRow;

public class W3CssStylingPage extends DirectAccessPage {

  public static String url = "https://www.w3schools.com/css/css_table.asp";
  private W3schoolsCSSTable table;

  public W3CssStylingPage(BaseDriver driver) {
    super(driver, "CSS Styling Tables");
    table = new W3schoolsCSSTable(driver);
  }

  public W3schoolsCSSTableRow getFirstRow() {
    return table.getFirstRow();
  }
}
