package pages;

import drivers.BaseDriver;
import interfaces.PageNavigator;

public class DirectAccessPage extends BasePage implements PageNavigator {

  public DirectAccessPage(BaseDriver driver, String title) {
    super(driver, title);
  }
}
