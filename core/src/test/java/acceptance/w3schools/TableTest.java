package acceptance.w3schools;

import static interfaces.PageNavigator.getPage;

import org.testng.annotations.Test;
import pages.example.w3schools.W3CssStylingPage;
import tests.BaseTest;

public class TableTest extends BaseTest {
  public TableTest() {
    super(TableTest.class);
  }

  @Test
  public void openMainPage() throws Exception {
    getPage(getDriver(), W3CssStylingPage.class).getFirstRow();
  }
}
