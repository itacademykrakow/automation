package acceptance.itacademy;

import static interfaces.PageNavigator.getPage;

import org.testng.annotations.Test;
import pages.example.itacademy.ITAcademyHomePage;
import tests.BaseTest;

public class OpenPageTest extends BaseTest {

  public OpenPageTest() {
    super(OpenPageTest.class);
  }

  @Test
  public void openMainPage() throws Exception {
    getPage(getDriver(), ITAcademyHomePage.class).navigateToSzkolenia();
  }
}
