package acceptance.rest;

import org.springframework.util.Assert;
import org.testng.annotations.Test;
import tests.BaseTest;
import utils.restClient.RestHttpClient;

public class RestApiTest extends BaseTest {

  public RestApiTest() {
    super(RestApiTest.class);
  }

  @Test
  public void getIp() {
    Assert.isTrue(
        RestHttpClient.get("http://ip.jsontest.com/").getBody().contains("ip"),
        "verify response contains 'ip'");
  }

  @Test
  public void getHttpBin() {
    Assert.isTrue(
        RestHttpClient.get("https://httpbin.org/get").getBody().contains("headers"),
        "verify response contains 'headers'");
  }

  @Test
  public void postHttpBin() {
    Assert.isTrue(
        RestHttpClient.post("https://httpbin.org/post").getBody().contains("headers"),
        "verify response contains 'headers'");
  }
}
